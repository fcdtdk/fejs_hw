class Human {
    constructor(name, surname, sex, age) {
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.age = age;
    }
    getHuman() {
        return (`${this.name} ${this.surname}; пол: ${this.sex}; возраст:  ${this.age}.`);
    }
}

let arrHuman = new Array(parseInt(prompt('Сколько людей?:', 5)));

function setArrHuman() {
    for (let i = 0; i < arrHuman.length; i++) {
        arrHuman[i] = new Human();
        for (let prop in arrHuman[i]) {
            arrHuman[i][prop] = prompt('Укажи ' + prop + ' человека');
        }
    }
}

function getArrHuman() {
    for (let i = 0; i < arrHuman.length; i++) {
        document.write(arrHuman[i].getHuman() + '; </br>');
    }
    document.write('</br>');
}

function sortArrHuman(order = 'asc') {
    arrHuman.sort(function (a, b) {
        if (order == 'dsc') {
            return parseInt(b.age) - parseInt(a.age);
        } else {
            return parseInt(a.age) - parseInt(b.age);
        }
    })
}

setArrHuman();
getArrHuman();
sortArrHuman();
getArrHuman();
sortArrHuman('dsc');
getArrHuman();

class Doctor extends Human {
    constructor(specialty) {
        super();
        this.specialty = specialty;
    }
    setDoctor() {
        for (let prop in this) {
            this[prop] = prompt('Укажи ' + prop + ' врача');
        }
    }
    getDoctor() {
        return (`${super.getHuman()}; специальность:  ${this.specialty}`);
    }
}

let newDoc = new Doctor();
newDoc.setDoctor();
document.write(newDoc.getDoctor());

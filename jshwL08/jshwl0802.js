let li02 = document.getElementById('li02');
let txtinput = document.createElement('textarea');
let button = document.createElement('button');
let div = document.createElement('div');
// div.style.display = 'inline-block';
txtinput.setAttribute('placeholder', 'TEXT');
li02.insertAdjacentElement('beforeend', txtinput);
button.innerText = 'Copy to ...';
txtinput.insertAdjacentElement('afterend', button);
button.insertAdjacentElement('afterend', div);
button.onclick = () => {
    if (txtinput.value != '') {
        div.innerText = txtinput.value;
        txtinput.value = ''; 
    }
}

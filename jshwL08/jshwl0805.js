let li05 = document.getElementById('li05');
let div05 = document.createElement('div');
li05.insertAdjacentElement('beforeend', div05);
let input = document.createElement('input');
let br01 = document.createElement('br');
let br02 = document.createElement('br');
div05.insertAdjacentElement('beforeend', input);
input.classList.add('l0805');
// input.id.add('ipt0805');
input.setAttribute('id', 'ipt0805');
input.insertAdjacentElement('afterend', br01);
let str = 'Я добавлю ';
let signsTxt = ['+', '-', '/', '*'];
let signs = new Array(4);
for (let i = signs.length - 1; i >= 0; i--) {
    signs[i] = document.createElement('button');
    signs[i].classList.add('l0805');
    signs[i].innerText = str + signsTxt[i];
    br01.insertAdjacentElement('afterend', signs[i]);
}
signs[3].insertAdjacentElement('afterend', br02)
let digits = new Array(10);
for (let i = digits.length - 1; i >= 0; i--) {
    digits[i] = document.createElement('button');
    digits[i].classList.add('l0805');
    if (i != digits.length - 1) {
        digits[i].innerText = i + 1;
    } else {
        digits[i].innerText = 0;
    }
    br02.insertAdjacentElement('afterend', digits[i]);
}
for (let i = 0; i < digits.length; i++) {
    digits[i].onclick = () => {
        if (i != digits.length - 1) {
            add(i + 1);
        } else {
            add(0);
        }
    }
}
for (let i = 0; i < signs.length; i++) {
    signs[i].onclick = () => {
        add(signsTxt[i]);
    }
}
function add(text) {
    let inpt = document.getElementById('ipt0805');
    inpt.value = inpt.value + text;
}
let pArr = new Array(10);
let li04 = document.getElementById('li04');
for (let i = 0; i < pArr.length; i++) {
    let n = i + 1;
    pArr[i] = document.createElement('p');
    pArr[i].style.color = 'brown';
    pArr[i].innerText = 'Параграф - ' + n;
    li04.insertAdjacentElement('beforeend', pArr[i]);
}
pArr.forEach(function (e) {
    e.onmouseover = () => {
        e.style.color = 'magenta';
        e.style.cursor = 'pointer';
    }
    e.onmouseout = () => {
        e.style.color = 'brown';
        e.style.cursor = 'default';
    }
    e.onclick = () => {
        e.innerText = '';
    }
})
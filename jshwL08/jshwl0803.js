let li03 = document.getElementById('li03');
let div03 = document.createElement('div');
let img = document.createElement('img');
let btn = document.createElement('button');
let src1 = 'https://itproger.com/img/courses/1476977240.jpg';
let src2 = 'https://itproger.com/img/courses/1476977488.jpg';
let src3 = 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png'
img.style.width = '425px';
img.style.height = '280px';
img.src = src1;
let imgArr = [src1, src2, src3];
li03.insertAdjacentElement('beforeend', div03);
div03.insertAdjacentElement('afterbegin', img);
div03.insertAdjacentElement('afterend', btn);
btn.innerText = 'Изменить картинку';
let i = 1;
btn.onclick = () => {
    img.src = imgArr[i];
    i++;
    if (i > 2) {
        i = 0;
    }
}


let btn = document.createElement('button');
let divArr = new Array(100);
btn.innerText = 'Нарисовать круг!';
document.body.append(btn);
btn.classList.add('button');
getCursor(btn);
btn.insertAdjacentHTML('afterend', '</br>');
btn.addEventListener('click', () => {
    for (let i = 0; i < divArr.length; i++) {
        divArr[i] = document.createElement('div');
        divArr[i].id = `div${i}`;
        document.body.append(divArr[i]);
        divArr[i].style.display = 'inline-block';
        divArr[i].style.width = '25px';
        divArr[i].style.height = '25px';
        divArr[i].style.borderRadius = '50%';
        divArr[i].style.backgroundColor = getRandColor();
        getCursor(divArr[i]);
        divArr[i].onclick = () => {
            divArr[i].remove();
        }
    }
    btn.remove();
});
function getCursor(el) {
    el.onmouseover = () => {
        el.style.cursor = 'pointer';
    }
    el.onmouseout = () => {
        el.style.cursor = 'default';
    }
}
function getRandColor() {
    let color = Math.floor(Math.random() * Math.pow(256, 3)).toString(16);
    while (color.length < 6) {
        color = "0" + color;
    }
    return "#" + color;
}

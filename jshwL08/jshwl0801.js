let divs = new Array(5);
let li = document.getElementById('li01');
for (let i = 0; i < divs.length; i++) {
    divs[i] = document.createElement('div');
    divs[i].style.backgroundColor = 'yellow';
    divs[i].style.width = '40px';
    divs[i].style.height = '30px';
    divs[i].style.border = 'groove';
    divs[i].style.display = 'inline-block';
    li.insertAdjacentElement('beforeend', divs[i]);
}
setInterval(function () {
    setTimeout(function () {
        divs.forEach(function (el) {
            el.style.backgroundColor = 'green';
        })
    }, 1000);
    setTimeout(function () {
        divs.forEach(function (el) {
            el.style.backgroundColor = 'magenta';
        })
    }, 1500);
    setTimeout(function () {
        divs.forEach(function (el) {
            el.style.backgroundColor = 'yellow';
        })
    }, 2000);
},2000);
function filterBy(arr, type) {
    return arr.reduce((res, item) => { 
        if (typeof item != type) { 
            res.push(item);
        }
        return res
    }, []);
}

const tstArr = ['hello', 'world', 23, '23', null];

console.log(filterBy(tstArr, 'string'));
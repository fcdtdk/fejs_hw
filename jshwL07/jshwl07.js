class CreateNewUser {
    constructor(name, surname, birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }
    setLogin() {
        this.name = prompt('Введи имя');
        this.surname = prompt('Введи фамилию');
    }
    getLogin() {
        document.write(`Логин: ${this.name[0]}${this.surname}`.toLowerCase() + '</br>');
        console.log(`Логин: ${this.name[0]}${this.surname}`.toLowerCase());
    }
    setBirthday() {
        this.birthday = prompt('Дата рождения?:', '31.12.1970');
    }
    get nowDate() {
        let now = new Date();
        return now;
    }
    get bDate() {
        let dmy = this.birthday.split('.');
        let bdate = new Date(parseInt(dmy[2]),
                            parseInt(dmy[1]) - 1,
                            parseInt(dmy[0]));
        return bdate;
    }
    getAge() {
                                        // из мс в Годы:                            
        let age = parseInt((this.nowDate - this.bDate) / (365.25 * 24 * 60 * 60 * 1000));
        document.write('Возраст: ' + age + ' полных лет</br>');
        console.log('Возраст: ' + age + ' полных лет');
    }
    getPassword() {
        let pname = this.name[0].toUpperCase();
        let psurname = this.surname.toLowerCase();
        let pyear = this.bDate.getFullYear();
        document.write(`Пароль: ${pname}${psurname}${pyear} </br>`);
        console.log(`Пароль: ${pname}${psurname}${pyear}`);
    }
}

let newUser = new CreateNewUser();
newUser.setLogin();
newUser.getLogin();
newUser.setBirthday();
newUser.getAge();
newUser.getPassword();